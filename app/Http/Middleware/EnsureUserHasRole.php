<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EnsureUserHasRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ... $roles)
    {   

        $employee = $request->user();

        foreach($roles as $role) {
            // dd($user->hasRole($role,$user->id_user));
            // Check if user has the role This check will depend on how your roles are set up
            if(($employee->hasRole($role,$employee->id_user)))
                return $next($request);
        }


        return redirect('dashboard');
    }
}
