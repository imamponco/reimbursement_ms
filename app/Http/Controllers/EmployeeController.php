<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

use App\Models\Employee;
use App\Models\Benefit;
use App\Models\Tr_benefit;
use Yajra\Datatables\Datatables;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.employee.employee');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employee = Employee::orderBy('created_at','desc')->get();
        return Datatables::of($employee)->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation_value = [
            'name'            => 'required|regex:/^[a-zA-Z ]+$/|min:5|max:50',
            'email'           => 'required|email|min:5|max:30|unique:employee',
            'password'        => 'required|min:6|max:25',
            'repassword'      => 'required|same:password',
            'employeegender'  => 'required',
            'date_of_birth'   => 'required',
            'address'         => 'required',
            'position'        => 'required',
        ];

        $validator = Validator::make($request->all(),$validation_value);

        if ($validator->fails()) {
            return response()->json(['message'=>'Gagal menambah data','errors' => $validator->errors()],422);
        }

        $data = [
            "name"          => $request->name,
            "email"         => $request->email,
            "password"      => Hash::make($request->password),
            "gender"        => $request->employeegender,
            "date_of_birth" => $request->date_of_birth,
            "position"      => $request->position,
            "address"       => $request->address
        ];

        $insert = Employee::create($data);

        
        if($insert){

            //Initialise benefit for employee
            $benefitemployee = $request->benefitemployee;

            if(count($benefitemployee) > 0 ){

                for ($i=0; $i < count($benefitemployee); $i++) { 
                    
                    $current_benefit = Benefit::where('id_benefit',$benefitemployee[$i])->get()->first();

                    $tr_benefit = [
                        'id_employee'        => $insert->id_employee,
                        'id_benefit'         => $benefitemployee[$i],
                        'beginning_balance'  => $current_benefit->balance,
                        'adjustment_balance' => 0,
                        'used_balance'       => 0,
                        'current_balance'    => $current_benefit->balance,
                    ];

                    $insert_tr = Tr_benefit::create($tr_benefit);
                
                }

            }

            return response()->json(['message'=>'Berhasil menambah data!'],200);
        
        }else{
            return response()->json(['message'=>'Gagal menambah data','errors' => $insert],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $Employee = Employee::find($request->id);
        return response()->json($Employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee,Request $request)
    {
        $Employee = Employee::with('objtrbenefit')->find($request->id);
        return response()->json($Employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        $id = $request->id;

        $employee = Employee::where('id_employee',$id)->get()->first();

        $data = [
            "name"          => $request->name,
            "email"         => $request->email,
            "gender"        => $request->employeegender,
            "date_of_birth" => $request->date_of_birth,
            "position"      => $request->position,
            "address"       => $request->address
        ];

        $validation_value = [
            'name'            => 'required|regex:/^[a-zA-Z ]+$/|min:5|max:50',
            'email' =>
            [
                'required',
                'min:5',
                'max:30',
                Rule::unique('employee')->ignore($id,'id_employee')
            ],
            'employeegender'  => 'required',
            'date_of_birth'   => 'required',
            'address'         => 'required',
            'position'        => 'required',
        ];

        if(!empty($request->password)){
            $validation_value += [
                'password' => 'required|min:6|max:25',
                'repassword' => 'required|same:password'
            ];
            $data += [
                'password' => Hash::make($request->password)
            ];
        }

        if(!empty($request->email) ){
            if($request->email != $employee->email){
                $validation_value +=  [
                    'email'           => 'required|email|min:5|max:30|unique:employee',
                ];
                $data += [
                    'email' => $request->email
                ];
            }
        }else{
            $validation_value +=  [
                'email'           => 'required|email|min:5|max:30|unique:employee',
            ];
        }

        $validator = Validator::make($request->all(),$validation_value);

        if ($validator->fails()) {
            return response()->json(['message'=>'Gagal mengubah data','errors' => $validator->errors()],422);
        }

        $update = Employee::where('id_employee',$id)->update($data);

        if($update){

            $benefitemployee = ($request->benefitemployee == null ? [] : $request->benefitemployee );
      
            $current_benefit = Tr_benefit::select('id_benefit')
                                        ->where('id_employee',$id)
                                        ->where(DB::raw('YEAR(created_at)'), '=', Carbon::now()->format('Y'))
                                        ->get();

            $current_benefit = $current_benefit->map(function ($value) {
                return (string) $value["id_benefit"];
            })->toArray();
            
            $out = array_diff($current_benefit,$benefitemployee);
            $in  = array_diff($benefitemployee,$current_benefit);
            //Benefit In
            if(count($in)>0){

                foreach ($in as $key => $value) {
                    
                    $current_benefit = Benefit::where('id_benefit',$value)->get()->first();
                    
                    $tr_benefit = [
                        'id_employee'        => $id,
                        'id_benefit'         => $value,
                        'beginning_balance'  => $current_benefit->balance,
                        'adjustment_balance' => 0,
                        'used_balance'       => 0,
                        'current_balance'    => $current_benefit->balance,
                    ];

                    $insert_tr = Tr_benefit::create($tr_benefit);
                }
              
            }
           
            //Benefit Out
            if(count($out) > 0){
                foreach ($out as $key => $value) {
                    $out_tr = Tr_benefit::where('id_benefit',$value)->where('id_employee',$id)->first()->delete();
                } 
            }


            return response()->json(['message'=>'Berhasil mengubah data'],200);
        }else{
            return response()->json(['message'=>'Gagal mengubah data','errors' => $update],422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();
    }

    public function login_post(Request $request){

        $identity = $request->email;
        $password = $request->password;

        if (Auth::attempt([
            filter_var($identity, FILTER_VALIDATE_EMAIL) ? 'email' : 'name' => $identity, 'password' => $request->password
            ]))
        {
            return redirect()->route('dashboard');
        }else{
            return redirect()->route('login_view')
                            ->withErrors(['login' => 'The email or password you’ve entered is incorrect.!'])
                            ->withInput();
        }
    }

    public function logout(){
        Auth::logout();

        return redirect()->route('login_view')->withErrors(['logout' => 'Berhasil keluar dari sistem!']);
    }

}
