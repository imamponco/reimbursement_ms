<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

use App\Models\Benefit;
use App\Models\Tr_benefit;
use App\Models\Employee;

use Yajra\Datatables\Datatables;

class TrBenefitController extends Controller
{
    //
    public function create(){
        
        $tr_benefit = Tr_benefit::with('objbenefit')->orderBy('created_at','desc')->get();

        return Datatables::of($tr_benefit)->editColumn('created_at', function($query) {
            return $query->created_at->format('Y');
        })->make(true);
        
    }
}
