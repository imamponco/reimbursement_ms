<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

use App\Models\Benefit;

use Yajra\Datatables\Datatables;

class BenefitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('master.benefit.benefit');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $benefit = Benefit::orderBy('created_at','desc')->get();
        return Datatables::of($benefit)->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation_value = [
            'benefit_name'     => 'required|min:5|max:50',
            'balance'         => 'required|integer|gt:0',
        ];

        $validator = Validator::make($request->all(),$validation_value);

        if ($validator->fails()) {
            return response()->json(['message'=>'Gagal menambah data','errors' => $validator->errors()],422);
        }

        $data = [
            "benefit_name" => $request->benefit_name,
            "balance"     => $request->balance,
        ];

        $insert = Benefit::create($data);

        if($insert){

            return response()->json(['message'=>'Berhasil menambah data!'],200);
        
        }else{
            return response()->json(['message'=>'Gagal menambah data','errors' => $insert],422);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Benefit  $benefit
     * @return \Illuminate\Http\Response
     */
    public function show(Benefit $benefit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Benefit  $benefit
     * @return \Illuminate\Http\Response
     */
    public function edit(Benefit $benefit,Request $request)
    {
        $Benefit = Benefit::find($request->id);
        return response()->json($Benefit);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Benefit  $benefit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Benefit $benefit)
    {
        
        $validation_value = [
            'benefit_name'     => 'required|min:5|max:50',
            'balance'         => 'required|integer|gt:0',
        ];

        $validator = Validator::make($request->all(),$validation_value);

        if ($validator->fails()) {
            return response()->json(['message'=>'Gagal menambah data','errors' => $validator->errors()],422);
        }

        $data = [
            "benefit_name" => $request->benefit_name,
            "balance"     => $request->balance,
        ];

        $insert = Benefit::where('id_benefit',$request->id)->update($data);

        
        if($insert){

            return response()->json(['message'=>'Berhasil mengubah data!'],200);
        
        }else{
            return response()->json(['message'=>'Gagal mengubah data','errors' => $insert],422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Benefit  $benefit
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $benefit = Benefit::find($id);
        $benefit->delete();
    }
}
