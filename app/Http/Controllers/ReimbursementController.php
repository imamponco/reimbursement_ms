<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Carbon\Carbon;

use App\Models\Benefit;
use App\Models\Tr_benefit;
use App\Models\Employee;
use App\Models\Reimbursement;

use Yajra\Datatables\Datatables;
use Intervention\Image\ImageManagerStatic as Image;

class ReimbursementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('reimbursement.request.request');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        $reimbursement = Reimbursement::orderBy('created_at','desc')->get();

        return Datatables::of($reimbursement)->editColumn('created_at', function($query) {
                            return $query->created_at->format('Y-M-d');
                        })->addColumn('year', function($query) {
                            return $query->created_at->format('Y');
                        })->make(true);
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $validation_value = [
            'description'      => 'required|min:6|max:255',
            'total_claim'      => 'required|integer|gt:0',
            'benefit'          => 'required',
            "prove"            => "required|mimes:jpg,bmp,png|max:500000"
        ];

        $validator = Validator::make($request->all(),$validation_value);

        if ($validator->fails()) {
            return response()->json(['message'=>'Gagal menambah data','errors' => $validator->errors()],422);
        }

        $id_employee = Auth::user()->id_employee;

        /* PATH */
        $path = date("Y").'/'.date("m")."/";
        $destinationPath = public_path('assets/images/'.$path);
        
        if(!is_dir($destinationPath)) {
            mkdir($destinationPath, 0775, true);
        }

        /* COMPRESS IMAGE */
        if(!empty($request->prove)){
            $img = Image::make($request->prove);
        
            $img->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $imgname = $id_employee."-".time(). '.' . $request->prove->getClientOriginalExtension();
        }else{
            $img = false;
            $imgname = $destinationPath.'default.jpg';
        }
        /* END COMPRESS IMAGE */
        
        /* UPLOAD IMAGE*/
        if($img){
            $img->save($destinationPath.$imgname,100);
            $imgname = $path.$imgname;
        }else{
            return response()->json(['message'=>'Gagal menyimpan gambar.'],422);
        }
        /* END UPLOAD IMAGE AND FILE */

        $data = [
            "description"      => $request->description,
            "total_claim"      => $request->total_claim,
            "id_employee"      => $id_employee,
            "id_benefit"       => $request->benefit,
            "claim_status"     => 0,
            "prove"            => $imgname,
            "reference_number" => null,
        ];

        $insert = Reimbursement::create($data);

        if($insert){
            return response()->json(['message'=>'Berhasil menambah data!'],200);
        }else{
            return response()->json(['message'=>'Gagal menambah data','errors' => $insert],422);
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reimbursement  $reimbursement
     * @return \Illuminate\Http\Response
     */
    public function show(Reimbursement $reimbursement)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reimbursement  $reimbursement
     * @return \Illuminate\Http\Response
     */
    public function edit(Reimbursement $reimbursement,Request $request)
    {
        $reimbursement = Reimbursement::find($request->id);
        return response()->json($reimbursement);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reimbursement  $reimbursement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reimbursement $reimbursement)
    {
        $id = $request->id;

        $validation_value = [
            'description'      => 'required|min:6|max:255',
            'total_claim'      => 'required|integer|gt:0',
            'benefit'          => 'required',
            "prove"            => "required|mimes:jpg,bmp,png|max:500000"
        ];

        $validator = Validator::make($request->all(),$validation_value);

        if ($validator->fails()) {
            return response()->json(['message'=>'Gagal mengubah data','errors' => $validator->errors()],422);
        }

        $id_employee = Auth::user()->id_employee;

        /* PATH */
        $path = date("Y").'/'.date("m")."/";
        $destinationPath = public_path('assets/images/'.$path);
        
        if(!is_dir($destinationPath)) {
            mkdir($destinationPath, 0775, true);
        }

        /* COMPRESS IMAGE */
        if(!empty($request->prove)){

            $img = Image::make($request->prove);
        
            $img->resize(400, 400, function ($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            });

            $imgname = $id_employee."-".time(). '.' . $request->prove->getClientOriginalExtension();
        }else{
            $img = false;
            $imgname = $destinationPath.'default.jpg';
        }
        /* END COMPRESS IMAGE */

        $current_img = Reimbursement::find($request->id);
        
        /* UPLOAD IMAGE*/
        if($img){
          
            
            if(file_exists(public_path("assets/images/").$current_img->prove)){     
                unlink(public_path("assets/images/").$current_img->prove);
            }
         
            $img->save($destinationPath.$imgname,100);           
            $imgname = $path.$imgname;
        }else{
            $imgname = $current_img->prove;
        }
        /* END UPLOAD IMAGE AND FILE */

        $data = [
            "description"      => $request->description,
            "total_claim"      => $request->total_claim,
            "id_employee"      => $id_employee,
            "id_benefit"       => $request->benefit,
            "claim_status"     => 0,
            "prove"            => $imgname,
            "reference_number" => null,
        ];

        $update = Reimbursement::where('id_reimbursement',$id)->update($data);

        if($update){
            return response()->json(['message'=>'Berhasil mengubah data!'],200);
        }else{
            return response()->json(['message'=>'Gagal mengubah data','errors' => $update],422);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reimbursement  $reimbursement
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $reimbursement = Reimbursement::find($id);

        if(file_exists(public_path("assets/images/").$reimbursement->prove)){     
            unlink(public_path("assets/images/").$reimbursement->prove);
        }

        $reimbursement->delete();
    }

    public function index_validation(){
        return view('reimbursement.validation.validation');
    }

    public function approve(Request $request){

        $id = $request->id;

        $reimbursement = Reimbursement::find($id);

        $id_benefit = $reimbursement->id_benefit;
        $id_employee = $reimbursement->id_employee;

        $data = [
            "claim_status"      => 1,
            "reference_number"  => $request->reference_number
        ];
        $update = Reimbursement::where('id_reimbursement',$id)->update($data);
        
        if($update){
            
            $used_balance = Reimbursement::select(DB::raw('SUM(total_claim) as used_balance'))->where('id_employee',$id_employee)->where('id_benefit',$id_benefit)->first();

            if(!empty($used_balance)){
                $used_balance = $used_balance->used_balance;
            }

            $current_balance = Tr_benefit::where('id_employee',$id_employee)
                                          ->where('id_benefit',$id_benefit)
                                          ->where(DB::raw('YEAR(created_at)'), '=', Carbon::now()->format('Y'))
                                          ->first();

        
            if(!empty($current_balance)){
                $current_balance = $current_balance->current_balance;
            }


            $current_balance = $current_balance - $used_balance;

            if($current_balance <= 0 ){
                $current_balance = 0;
            }

            $update_tr = [
                'used_balance'  => $used_balance,
                'current_balance' => $current_balance
            ];

            $update_tr_benefit = Tr_benefit::where('id_employee',$id_employee)
                                            ->where('id_benefit',$id_benefit)
                                            ->where(DB::raw('YEAR(created_at)'), '=', Carbon::now()->format('Y'))
                                            ->update($update_tr);

            return response()->json(['message'=>'Berhasil melakukan approve data!'],200);
        }else{
            return response()->json(['message'=>'Gagal melakukan approve data','errors' => $update],422);
        }
    }

    public function reject($id){

        $data = [
            "claim_status"     => 2,
        ];

        $update = Reimbursement::where('id_reimbursement',$id)->update($data);

        if($update){
            return response()->json(['message'=>'Berhasil melakukan reject data!'],200);
        }else{
            return response()->json(['message'=>'Gagal melakukan reject data','errors' => $update],422);
        }
    }
}
