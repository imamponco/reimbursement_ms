<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reimbursement extends Model
{
    use HasFactory;

    protected $table = 'reimbursement';

    protected $primaryKey = 'id_reimbursement';

    public $incrementing = true;

    public $fillable = [
        'id_employee',
        'id_benefit',
        'total_claim',
        'description',
        'claim_status',
        'prove',
        'reference_number',
        'created_at',
        'updated_at'
    ];
}
