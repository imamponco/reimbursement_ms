<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class Employee extends Authenticatable 
{
    use HasFactory;
    
    protected $table = 'employee';
    
    protected $primaryKey = 'id_employee';
    
    public $incrementing = true;
    
    public $timestamps = true;
    
    protected $hidden = [
        'password',
    ];

    public $fillable = [
        'email',
        'password',
        'name',
        'gender',
        'date_of_birth',
        'address',
        'position',
        'created_at',
        'updated_at'
    ];

    public function hasRole($role,$id)
    {
        return Employee::where('position', $role)->where('id_employee',$id)->get();
    }

    public function objtrbenefit()
    {
        return $this->hasMany(Tr_benefit::class, 'id_employee', 'id_employee');
    }

}
