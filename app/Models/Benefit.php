<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Benefit extends Model
{
    use HasFactory;

    protected $table = 'benefit';

    protected $primaryKey = 'id_benefit';

    public $incrementing = true;

    public $fillable = [
        'benefit_name',
        'balance',
        'created_at',
        'updated_at'
    ];
}
