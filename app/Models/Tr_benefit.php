<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Tr_benefit extends Model
{
    use HasFactory;

    protected $table = 'tr_benefit';

    protected $primaryKey = 'id_benefit';

    public $incrementing = true;

    public $fillable = [
        'id_employee',
        'id_benefit',
        'beginning_balance',
        'adjustment_balance',
        'used_balance',
        'current_balance',
        'created_at',
        'updated_at'
    ];

    protected static function booted()
    {
        static::addGlobalScope('filterdata', function ($benefit) {
          
            $auth = Auth::user();
            
            $position = $auth->position;
            
            if($position == 'employee'){
                $benefit->where('id_employee',$auth->id_employee);
            }else{
                $benefit->where('id_employee',$auth->id_employee);
            }
            
        });
    }

    public function objbenefit()
    {
        return $this->belongsTo(Benefit::class, 'id_benefit', 'id_benefit');
    }

    public function objemployee()
    {
        return $this->belongsTo(Employee::class, 'id_employee', 'id_employee');
    }
}
