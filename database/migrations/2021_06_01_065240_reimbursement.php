<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Reimbursement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reimbursement', function (Blueprint $table) {
            $table->bigIncrements('id_reimbursement');
            $table->string('id_employee',11)->nullable();
            $table->string('id_benefit',11)->nullable();
            $table->double('total_claim',12)->nullable();
            $table->string('description',255)->nullable();
            $table->boolean('claim_status',1)->nullable();
            $table->string('prove',255)->nullable();
            $table->string('reference_number',50)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reimbursement');
    }
}
