<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TrBenefit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_benefit', function (Blueprint $table) {
            $table->bigIncrements('id_tr_benefit');
            $table->string('id_employee',11);
            $table->string('id_benefit',11);
            $table->double('beginning_balance',12);
            $table->double('adjustment_balance',12);
            $table->double('used_balance',12);
            $table->double('current_balance',12);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_benefit');
    }
}
