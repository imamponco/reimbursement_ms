<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Employee extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee', function (Blueprint $table) {
            $table->bigIncrements('id_employee');
            $table->string('email',35)->unique();
            $table->string('password',72)->nullable();
            $table->string('name',75)->nullable();
            $table->boolean('gender',1)->nullable();
            $table->date('date_of_birth')->nullable();
            $table->string('address',255)->nullable();
            $table->string('position',20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee');
    }
}
