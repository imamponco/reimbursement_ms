<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Employee;
use App\Models\Tr_benefit;
use Hash;
use Carbon\Carbon;

class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Employee::create([
            'email'=>'admin@gmail.com',
            'password' => Hash::make('qwe123'),
            'name'=>'Super Admin',
            'gender'=>'1',
            'date_of_birth'=> Carbon::now(),
            'address'=>'Jl.Kemang Raya No.26 B',
            'position'=>'superadmin',
        ]);

        Tr_benefit::create([
            'id_employee'        => 1,
            'id_benefit'         => 1,
            'beginning_balance'  => 4000000,
            'adjustment_balance' => 0,
            'used_balance'       => 0,
            'current_balance'    => 4000000,
        ]);
    }
}
