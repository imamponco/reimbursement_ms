<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* =========== MIDDLEWARE AUTH =========== */
Route::middleware(['web'])->group(function () {
    
        /* ===== LOGIN PAGE ===== */
    Route::get('/', function () {
        return view('auth.login');
    })->name('login_view')->middleware('guest');

    Route::post('login','EmployeeController@login_post')->name('login');

    Route::middleware(['auth'])->group(function () {
        /* =========== DASHBOARD =========== */
        Route::get('dashboard','DashboardController@index')->name('dashboard');

        /* =========== AUTH =========== */
        Route::get('logout','EmployeeController@logout')->name('logout');
        /* =========== END AUTH =========== */

        /* =========== MASTER =========== */
        Route::middleware(['role:superadmin'])->group(function () {
            Route::resources([
                'employee'           => 'EmployeeController',
                'benefit'            => 'BenefitController',
            ]);
        });

        Route::middleware(['role:superadmin,finance,employee'])->group(function () {
            Route::resources([
                'claim'      => 'ReimbursementController',
            ]);

            Route::get('get_tr_benefit','TrBenefitController@create')->name('get_tr');
        });

        Route::middleware(['role:superadmin,finance'])->group(function () {
            Route::get('index_validation','ReimbursementController@index_validation')->name('index_val');
            Route::patch('approve/{approve}','ReimbursementController@approve')->name('approve');
            Route::delete('reject/{id}','ReimbursementController@reject')->name('reject');
        });
        
    });

});