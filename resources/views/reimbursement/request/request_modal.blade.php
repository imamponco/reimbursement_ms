@section('modal')
<!-- Modal Starts Insert Data Reimbursement -->
<div class="modal fade" id="showModalInsertReimbursement" tabindex="-1" role="dialog"
aria-labelledby="ReimbursementModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 30px;">
        <div class="modal-header">
            <h5 class="modal-title" id="ReimbursementModalLabel">Reimbursement</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form class="forms-sample" method="POST" action="#" id="formInputReimbursement" enctype="multipart/form-data">
            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Description
                                    <span class="" style="color:red;">*</span>
                            </label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_description" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="text"
                                    class="form-control form-control-sm" name="description"
                                    id="description" placeholder="Description"
                                    aria-label="Description"
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Benefit</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_benefit" style="padding: 2px; color:red;"></div>
                                <select class="form-control selectWrapper" name="benefit"
                                id="benefit">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Total Claim</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_total_claim" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="number" min="0"
                                    class="form-control form-control-sm" name="total_claim"
                                    id="total_claim" placeholder="Total Claim"
                                    aria-label="Total Claim" value=""  />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Prove</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_prove" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="file"
                                    class="form-control form-control-sm" name="prove"
                                    id="prove" placeholder="prove"
                                    aria-label="prove" value=""  />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-success btn-fw"
                    style="border-radius: 21px;">Submit</button>
                <button type="button" class="btn btn-outline-danger btn-fw"
                    style="border-radius: 21px;" data-dismiss="modal"
                    id="btnCloseModalReimbursementInsert">Cancel</button>
            </div>
        </form>
    </div>
</div>
</div>
<!-- Modal End Insert Data Reimbursement -->

<!-- Modal Start Update Data Reimbursement -->
<div class="modal fade" id="showModalUpdateReimbursement" tabindex="-1" role="dialog"
aria-labelledby="ReimbursementModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="ReimbursementModalLabel">Reimbursement</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form class="forms-sample" method="POST" action="#" id="formUpdateReimbursement" enctype="multipart/form-data">
            @method('patch')
            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
            <input type="hidden" id="idReimbursement" name="id" >
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Description
                                    <span class="" style="color:red;">*</span>
                            </label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_u_description" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="text"
                                    class="form-control form-control-sm" name="description"
                                    id="Updatedescription" placeholder="Description"
                                    aria-label="Description"
                                />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Benefit</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_u_benefit" style="padding: 2px; color:red;"></div>
                                <select class="form-control selectWrapper" name="benefit"
                                id="Updatebenefit">
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Total Claim</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_u_total_claim" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="number" min="0"
                                    class="form-control form-control-sm" name="total_claim"
                                    id="Updatetotal_claim" placeholder="Total Claim"
                                    aria-label="Total Claim" value=""  />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Prove</label>
                            <div class="col-sm-9">
                                <div id="current_img"></div>                            
                                <div class="text-left" id="error_u_prove" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="file"
                                    class="form-control form-control-sm" name="prove"
                                    id="Updateprove" placeholder="prove"
                                    aria-label="prove" value=""  />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-success btn-fw"
                    style="border-radius: 21px;">Update</button>
                <button type="button" class="btn btn-outline-danger btn-fw"
                    style="border-radius: 21px;" data-dismiss="modal"
                    id="btnCloseModalReimbursementUpdate">Cancel</button>
            </div>
        </form>
    </div>
</div>
</div>
<!-- Modal End Update Data Reimbursement -->
@endsection
