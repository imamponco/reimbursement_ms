@extends('layouts.dashboard')
@section('content')
<!-- Tittle -->
<div class="row">
    <div style="margin-bottom: 0.2rem;" class="col-12 grid-margin">
        <div style="height: 65px; border-radius: 15px;" class="card">
            <div class="card-body">
                <div style="display: flex; align-items: flex-start; justify-content: space-between; height: 40px;">
                    <h4 class="card-title" style="font-size: 2rem; font-weight: 300; margin-bottom: 0px;">Benefit</h4>
                    {{-- <button type="button" class="btn btn-success btn-rounded btn-icon" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-success" title="EDIT" data-target="#showModalUpdateAssetClass" style="margin-right:5px;"><i style="font-size:1.5rem; margin-left:-7px;" class="ti-pencil-alt"></i></button> --}}
                    {{-- <button style="margin: -0.2rem .05rem -1rem auto;" type="button" class="btn btn-primary btn-sm"
                        data-toggle="modal" data-target="#showModalInsertReimbursement"><i
                            class="ti-plus menu-icon"></i>&ensp;
                        ADD</button> --}}
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Table -->
<div class="row">
    <div style="margin-bottom: 0.2rem;" class="col-12 grid-margin">
        <div class="card" style="border-radius: 15px;">
                <div class="card-body">
                {{-- <h4 class="card-title">Data table</h4> --}}
                <div class="row">
                    <div class="col-12">
                    <div class="table-responsive">
                        <table id="trTable" class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Year</th>
                                <th>Name Benefit</th>
                                <th>Beginning Balance</th>
                                <th>Adjustment Balance</th>
                                <th>Used Balance</th>
                                <th>Current Balance</th>
                            </tr>
                        </thead>
                        </table>
                    </div>
                    </div>
                </div>
                </div>
            </div>
    </div>
</div>
{{-- Table End --}}

<!-- Tittle -->
<div class="row">
    <div style="margin-bottom: 0.2rem;" class="col-12 grid-margin">
        <div style="height: 65px; border-radius: 15px;" class="card">
            <div class="card-body">
                <div style="display: flex; align-items: flex-start; justify-content: space-between; height: 40px;">
                    <h4 class="card-title" style="font-size: 2rem; font-weight: 300; margin-bottom: 0px;">Reimbursement</h4>
                    {{-- <button type="button" class="btn btn-success btn-rounded btn-icon" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-success" title="EDIT" data-target="#showModalUpdateAssetClass" style="margin-right:5px;"><i style="font-size:1.5rem; margin-left:-7px;" class="ti-pencil-alt"></i></button> --}}
                    <button style="margin: -0.2rem .05rem -1rem auto;" type="button" class="btn btn-primary btn-sm"
                        data-toggle="modal" data-target="#showModalInsertReimbursement"><i
                            class="ti-plus menu-icon"></i>&ensp;
                        ADD</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Table -->
<div class="row">
    <div style="margin-bottom: 0.2rem;" class="col-12 grid-margin">
        <div class="card" style="border-radius: 15px;">
                <div class="card-body">
                {{-- <h4 class="card-title">Data table</h4> --}}
                <div class="row">
                    <div class="col-12">
                    <div class="table-responsive">
                        <table id="reimburseTable" class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Year</th>
                                <th>Claim Date</th>
                                <th>Total Claim</th>
                                <th>Description</th>
                                <th>Prove</th>
                                <th>Claim Status</th>
                                <th>Reference Number</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                    </div>
                    </div>
                </div>
                </div>
            </div>
    </div>
</div>
{{-- Table End --}}
@endsection

@section('jscustom')

<script>
    
    var mTable,trTable;

    $(document).ready(function () {
        //INITIALISE SELECT2
        get_benefit();
        $("#benefit").select2({
            tags: true,
            width: '19rem',
            dropdownParent: $("#formInputReimbursement")
        });
        $("#Updatebenefit").select2({
            tags: true,
            width: '19rem',
            dropdownParent: $("#formUpdateReimbursement")
        });

        
        //RENDER DATA TABLES
        trTable = $('#trTable').DataTable({
            responsive: true,
            processing: true,
            "language": {
                "processing": "<div class='dot-opacity-loader'></div>"
            },
            "order": [[ 0, "desc" ]],
            serverSide: true,
            ajax: "{{ route('get_tr')}}",
            dom: 'Bfrtip',
            buttons: [
              
            ],
            columns: [
                {  
                    data: 'id_tr_benefit',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'created_at',
                    name: 'created_at'
                },
                {  
                    data: 'objbenefit',
                    render: function (data, type, row, meta) {
                        if(data != null){
                            return data.benefit_name;
                        }else{
                            return null;
                        }
                    }
                },
                {
                    data: 'beginning_balance',
                    name: 'beginning_balance'
                },
                {
                    data: 'adjustment_balance',
                    name: 'adjustment_balance'
                },
                {
                    data: 'used_balance',
                    name: 'used_balance'
                },
                {
                    data: 'current_balance',
                    name: 'current_balance'
                }
            ]
        });

        mTable = $('#reimburseTable').DataTable({
            responsive: true,
            processing: true,
            "language": {
                "processing": "<div class='dot-opacity-loader'></div>"
            },
            "order": [[ 0, "desc" ]],
            serverSide: true,
            ajax: "{{ route('claim.create')}}",
            dom: 'Bfrtip',
            buttons: [
              
            ],
            columns: [
                {  
                    data: 'id_reimbursement',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'year',
                    name: 'year'
                },
                {
                    data: 'created_at',
                    name: 'created_at'
                },
                {
                    data: 'total_claim',
                    name: 'total_claim'
                },
                {
                    data: 'description',
                    name: 'description'
                },
                {  
                    data: 'prove',
                    render: function (data, type, row, meta) {
                        if(data != null){
                            return "<a href='{{ asset('assets/images') }}/"+data+"'>View</a>"
                        }else{
                            return null;
                        }
                    }
                },
                {  
                    data: 'claim_status',
                    render: function (data, type, row, meta) {
                        if(data != null){
                            if(data == 0 ){
                                return 'PENDING';
                            }else if(data == 1){
                                return 'APPROVED';
                            }else{
                                return 'REJECTED';
                            }
                        }else{
                            return null;
                        }
                    }
                },
                {
                    data: 'reference_number',
                    name: 'reference_number'
                },
                {
                    data: 'id_reimbursement',
                    render: function (data, type, row) {
                        let buttonEdit =
                            '<button type="button" class="btn btn-success btn-rounded btn-icon" data-toggle="modal" data-placement="buttom" data-custom-class="tooltip-success" title="EDIT" data-target="#showModalUpdateReimbursement" style="margin-right:5px;" onclick="buttonEdit(\'' + data + '\');"><i style="font-size:1.5rem; margin-left:-7px;" class="ti-pencil-alt"></i></button>';
                        let buttonHapus =
                            '<button type="button" class="btn btn-danger btn-rounded btn-icon" data-toggle="tooltip" data-placement="bottom" data-custom-class="tooltip-success" title="DELETE" onclick="delete_data(\''+
                            data +'\',\'claim\');"><i style="font-size:1.5rem; margin-left:-8px;" class="ti-trash"></i></button>';
                        if(row.claim_status == 1){
                            return "CLOSED";
                        }else{
                            return buttonEdit + buttonHapus;
                        }
                        
                    }
                }
            ]
        });

        //INSERT Departement
        $('#formInputReimbursement').on('submit', function (e) {
            e.preventDefault();

            var url,data,form;

            url = 'claim';           //For routing
            data =  new FormData($('#formInputReimbursement')[0]);    // Data Form
            form = 'Reimbursement';          //Reset Trigger and Close Modal

            // General form insert_data(url,data,form)
            insert_data_file(url,data,form);
        });

        //UPDATE Departement
        $('#formUpdateReimbursement').on('submit', function (e) {

            e.preventDefault();
            var url,data,form;

            url = 'claim';           //For routing
            data =  new FormData($('#formUpdateReimbursement')[0]);   // Data Form
            form = 'Reimbursement';          // Reset Trigger and Close Modal

            // General form update_data(url,data,form)
            update_data_file(url,data,form);
        });

    });

    function get_benefit(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        $.ajax({
            type: "GET",
            url: "{{route('benefit.create')}}",
            success: function (data) {
                $.each(data.data, function (index, value) {
                    $('#benefit').append('<option id=' + value.id_benefit + ' value=' + value.id_benefit +
                        '>' + value.benefit_name +'</option>')
                });
                $.each(data.data, function (index, value) {
                    $('#Updatebenefit').append('<option id=' + value.id_benefit + ' value=' + value.id_benefit +
                        '>' + value.benefit_name +'</option>')
                });
            },
            error: function(response) {
                try {
                    $.toast({
                        heading: 'Danger',
                        text: 'Gagal mendapatkan data terkait.',
                        showHideTransition: 'slide',
                        icon: 'error',
                        loaderBg: '#f2a654',
                        position: 'top-right'
                    })
                } catch (err) {

                }
            }
        });
    }

    //EDIT BUTTON VIEW
    function buttonEdit(id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            }
        });
        $.ajax({
            type: "GET",
            url: "{{ route('claim.edit',['claim'=>1]) }}",
            data: {
                id: id
            },
            success: function (data) {
                if (data) {

                    clear_error();
                    
                    $("#idReimbursement").val(data.id_reimbursement);
                    $("#Updatedescription").val(data.description)
                    $("#Updatetotal_claim").val(data.total_claim)
                    if(data.prove != null){
                        $("#current_img").html("<a href='{{ asset('assets/images') }}/"+data.prove+"'>Current Img</a>")
                    }
                } 
            },
            error: function(response) {
                try {
                    $.toast({
                        heading: 'Danger',
                        text: 'Gagal mendapatkan data terkait.',
                        showHideTransition: 'slide',
                        icon: 'error',
                        loaderBg: '#f2a654',
                        position: 'top-right'
                    })
                } catch (err) {

                }

            }
        });
    }

</script>

@endsection

@include('reimbursement.request.request_modal')