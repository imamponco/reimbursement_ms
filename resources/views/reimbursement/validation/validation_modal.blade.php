@section('modal')
<!-- Modal Start Update Data Approve -->
<div class="modal fade" id="showModalUpdateApprove" tabindex="-1" role="dialog"
aria-labelledby="ApproveModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="ApproveModalLabel">Approve</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form class="forms-sample" method="POST" action="#" id="formUpdateApprove" enctype="multipart/form-data">
            @method('patch')
            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
            <input type="hidden" id="idApprove" name="id" >
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Reference Number
                                    <span class="" style="color:red;">*</span>
                            </label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_u_reference_number" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="text"
                                    class="form-control form-control-sm" name="reference_number"
                                    id="Updatereference_number" placeholder="Reference Number"
                                    aria-label="Reference Number"
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-success btn-fw"
                    style="border-radius: 21px;">Update</button>
                <button type="button" class="btn btn-outline-danger btn-fw"
                    style="border-radius: 21px;" data-dismiss="modal"
                    id="btnCloseModalApproveUpdate">Cancel</button>
            </div>
        </form>
    </div>
</div>
</div>
<!-- Modal End Update Data Approve -->
@endsection
