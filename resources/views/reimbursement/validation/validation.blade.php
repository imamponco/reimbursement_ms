@extends('layouts.dashboard')
@section('content')


<!-- Tittle -->
<div class="row">
    <div style="margin-bottom: 0.2rem;" class="col-12 grid-margin">
        <div style="height: 65px; border-radius: 15px;" class="card">
            <div class="card-body">
                <div style="display: flex; align-items: flex-start; justify-content: space-between; height: 40px;">
                    <h4 class="card-title" style="font-size: 2rem; font-weight: 300; margin-bottom: 0px;">Reimbursement</h4>
                    {{-- <button type="button" class="btn btn-success btn-rounded btn-icon" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-success" title="EDIT" data-target="#showModalUpdateAssetClass" style="margin-right:5px;"><i style="font-size:1.5rem; margin-left:-7px;" class="ti-pencil-alt"></i></button> --}}
                    <button style="margin: -0.2rem .05rem -1rem auto;" type="button" class="btn btn-primary btn-sm"
                        data-toggle="modal" data-target="#showModalInsertReimbursement"><i
                            class="ti-plus menu-icon"></i>&ensp;
                        ADD</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Table -->
<div class="row">
    <div style="margin-bottom: 0.2rem;" class="col-12 grid-margin">
        <div class="card" style="border-radius: 15px;">
                <div class="card-body">
                {{-- <h4 class="card-title">Data table</h4> --}}
                <div class="row">
                    <div class="col-12">
                    <div class="table-responsive">
                        <table id="reimburseTable" class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Year</th>
                                <th>Claim Date</th>
                                <th>Total Claim</th>
                                <th>Description</th>
                                <th>Prove</th>
                                <th>Claim Status</th>
                                <th>Reference Number</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                    </div>
                    </div>
                </div>
                </div>
            </div>
    </div>
</div>
{{-- Table End --}}
@endsection

@section('jscustom')

<script>
    
    var mTable;

    $(document).ready(function () {
        
        mTable = $('#reimburseTable').DataTable({
            responsive: true,
            processing: true,
            "language": {
                "processing": "<div class='dot-opacity-loader'></div>"
            },
            "order": [[ 0, "desc" ]],
            serverSide: true,
            ajax: "{{ route('claim.create')}}",
            dom: 'Bfrtip',
            buttons: [
              
            ],
            columns: [
                {  
                    data: 'id_reimbursement',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'year',
                    name: 'year'
                },
                {
                    data: 'created_at',
                    name: 'created_at'
                },
                {
                    data: 'total_claim',
                    name: 'total_claim'
                },
                {
                    data: 'description',
                    name: 'description'
                },
                {  
                    data: 'prove',
                    render: function (data, type, row, meta) {
                        if(data != null){
                            return "<a href='{{ asset('assets/images') }}/"+data+"'>View</a>"
                        }else{
                            return null;
                        }
                    }
                },
                {  
                    data: 'claim_status',
                    render: function (data, type, row, meta) {
                        if(data != null){
                            if(data == 0 ){
                                return 'PENDING';
                            }else if(data == 1){
                                return 'APPROVED';
                            }else{
                                return 'REJECTED';
                            }
                        }else{
                            return null;
                        }
                    }
                },
                {
                    data: 'reference_number',
                    name: 'reference_number'
                },
                {
                    data: 'id_reimbursement',
                    render: function (data, type, row) {
                        let buttonApprove =
                            '<button type="button" class="btn btn-success btn-rounded btn-icon" data-toggle="modal" data-placement="buttom" data-custom-class="tooltip-success" title="Approve" data-target="#showModalUpdateApprove" style="margin-right:5px;" onclick="buttonEdit(\'' + data + '\');"><i style="font-size:1.5rem; margin-left:-7px;" class="ti-check"></i></button>';
                        let buttonReject =
                            '<button type="button" class="btn btn-danger btn-rounded btn-icon" data-toggle="tooltip" data-placement="bottom" data-custom-class="tooltip-success" title="Reject" onclick="delete_data(\''+
                            data +'\',\'reject\');"><i style="font-size:1.5rem; margin-left:-8px;" class="ti-close"></i></button>';
                        
                        if(row.claim_status == 1 || row.claim_status == 2 ){
                            return 'CLOSED'
                        }

                        return buttonApprove + buttonReject;
                    }
                }
            ]
        });

        //UPDATE Departement
        $('#formUpdateApprove').on('submit', function (e) {

            e.preventDefault();
            var url,data,form;

            url = 'approve';           //For routing
            data =  $(this).serialize();   // Data Form
            form = 'Approve';          // Reset Trigger and Close Modal

            swal({
                title: "Apakah Kamu Yakin?",
                text: "Klik OK, Maka Data Kamu Ubah Tidak Dapat DiKembalikan",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
            .then((approve) => {
                if (approve) {
                    // General form update_data(url,data,form)
                    update_data(url,data,form);
                }
            });
            
        });

    });

    //

    //EDIT BUTTON VIEW
    function buttonEdit(id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            }
        });
        $.ajax({
            type: "GET",
            url: "{{ route('claim.edit',['claim'=>1]) }}",
            data: {
                id: id
            },
            success: function (data) {
                if (data) {

                    clear_error();
                    
                    $("#idApprove").val(data.id_reimbursement);

                } 
            },
            error: function(response) {
                try {
                    $.toast({
                        heading: 'Danger',
                        text: 'Gagal mendapatkan data terkait.',
                        showHideTransition: 'slide',
                        icon: 'error',
                        loaderBg: '#f2a654',
                        position: 'top-right'
                    })
                } catch (err) {

                }

            }
        });
    }

</script>

@endsection

@include('reimbursement.validation.validation_modal')