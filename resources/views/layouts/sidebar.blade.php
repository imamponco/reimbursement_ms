<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="{{ url('dashboard') }}">
          <i class="ti-home menu-icon"></i>
          <span class="menu-title">Dashboard</span>
        </a>
      </li>
      @if(Auth::user()->position == 'superadmin')
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#ui-advanced" aria-expanded="false" aria-controls="ui-advanced">
          <i class="ti-view-list menu-icon"></i>
          <span class="menu-title">Master</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="ui-advanced">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item"> <a class="nav-link" href="{{ route('employee.index') }}">Employee</a></li>
            <li class="nav-item"> <a class="nav-link" href="{{ route('benefit.index') }}">Benefit</a></li>
            {{-- <li class="nav-item"> <a class="nav-link" href="{{ route('account.index') }}">Employee</a></li> --}}
          </ul>
        </div>
      </li>
      @endif
     
      <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
          <i class="ti-clipboard menu-icon"></i>
          <span class="menu-title">Reimbursement</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="form-elements">
          <ul class="nav flex-column sub-menu">
            @if(Auth::user()->position != 'finance')
              <li class="nav-item"> <a class="nav-link" href="{{route('claim.index')}}">Request</a></li>
            @endif
            @if(Auth::user()->position != 'employee')
              <li class="nav-item"><a class="nav-link" href="{{route('index_val')}}">Validation</a></li>
            @endif
          </ul>
        </div>
      </li>
    </ul>
  </nav>
