@section('modal')
<!-- Modal Starts Insert Data Benefit -->
<div class="modal fade" id="showModalInsertBenefit" tabindex="-1" role="dialog"
aria-labelledby="BenefitModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content" style="border-radius: 30px;">
        <div class="modal-header">
            <h5 class="modal-title" id="BenefitModalLabel">Benefit</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form class="forms-sample" method="POST" action="#" id="formInputBenefit">
            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Name
                                    <span class="" style="color:red;">*</span>
                            </label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_benefit_name" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="text"
                                    class="form-control form-control-sm" name="benefit_name"
                                    id="name" placeholder="Benefit Name"
                                    aria-label="Benefit Name"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Balance</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_balance" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="number"
                                    class="form-control form-control-sm" min="0" name="balance"
                                    id="balance" placeholder="Balance"
                                    aria-label="balance" value=""  />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-success btn-fw"
                    style="border-radius: 21px;">Submit</button>
                <button type="button" class="btn btn-outline-danger btn-fw"
                    style="border-radius: 21px;" data-dismiss="modal"
                    id="btnCloseModalBenefitInsert">Cancel</button>
            </div>
        </form>
    </div>
</div>
</div>
<!-- Modal End Insert Data Benefit -->

<!-- Modal Start Update Data Benefit -->
<div class="modal fade" id="showModalUpdateBenefit" tabindex="-1" role="dialog"
aria-labelledby="BenefitModalLabel" aria-hidden="true">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="BenefitModalLabel">Benefit</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form class="forms-sample" method="POST" action="#" id="formUpdateBenefit">
            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
            <input type="hidden" id="idBenefit" name="id" >
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Name
                                    <span class="" style="color:red;">*</span>
                            </label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_u_benefit_name" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="text"
                                    class="form-control form-control-sm" name="benefit_name"
                                    id="Updatebenefitname" placeholder="Benefit Name"
                                    aria-label="Benefit Name"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Balance</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_u_balance" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="number"
                                    class="form-control form-control-sm" min="0" name="balance"
                                    id="Updatebalance" placeholder="Balance"
                                    aria-label="balance" value=""  />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-success btn-fw"
                    style="border-radius: 21px;">Update</button>
                <button type="button" class="btn btn-outline-danger btn-fw"
                    style="border-radius: 21px;" data-dismiss="modal"
                    id="btnCloseModalBenefitUpdate">Cancel</button>
            </div>
        </form>
    </div>
</div>
</div>
<!-- Modal End Update Data Benefit -->
@endsection
