@section('modal')
<!-- Modal Starts Insert Data Employee -->
<div class="modal fade" id="showModalInsertEmployee" tabindex="-1" role="dialog"
aria-labelledby="EmployeeModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="border-radius: 30px;">
        <div class="modal-header">
            <h5 class="modal-title" id="EmployeeModalLabel">Employee</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form class="forms-sample" method="POST" action="#" id="formInputEmployee">
            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Name
                                    <span class="" style="color:red;">*</span>
                            </label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_name" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="text"
                                    class="form-control form-control-sm" name="name"
                                    id="name" placeholder="Employee Name"
                                    aria-label="Employee Name"
                                    oninput="this.value = this.value.toUpperCase()"  />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_email" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="email"
                                    class="form-control form-control-sm" name="email"
                                    id="email" placeholder="Email"
                                    aria-label="Email" value=""  />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Password</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_password" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="password"
                                    class="form-control form-control-sm" name="password"
                                    id="password" placeholder="password"
                                    aria-label="password" value=""  />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Re-Password</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_repassword" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="password"
                                    class="form-control form-control-sm" name="repassword"
                                    id="repassword" placeholder="repassword"
                                    aria-label="repassword" value=""  />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;"
                                class="col-sm-3 col-form-label">Gender
                            </label>
                            <div class="col-sm-8" >
                                <div class="text-left" id="error_employeegender" style="padding: 2px; color:red;"></div> 
                                <select class="form-control selectWrapper" name="employeegender"
                                id="employeegender" >
                                    <option value="">Select Gender</option>
                                    <option value="1">Man</option> 
                                    <option value="0">Women</option> 
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;"
                                class="col-sm-3 col-form-label">Date of Birth</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_date_of_birth" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="date"
                                    class="form-control form-control-sm" name="date_of_birth"
                                    id="date_of_birth" placeholder="date_of_birth" aria-label="date_of_birth"
                                    value=""  />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;"
                                class="col-sm-3 col-form-label">Position
                            </label>
                            <div class="col-sm-8" >
                                <div class="text-left" id="error_position" style="padding: 2px; color:red;"></div>
                                <select class="form-control selectWrapper" name="position"
                                id="position" >
                                    <option value="">Select Position</option>
                                    <option value="superadmin">Super Admin</option>
                                    <option value="employee">Employee</option> 
                                    <option value="finance">Finance</option> 
                                </select>
                            </div>    
                        </div>
                    </div>
                    
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Benefit</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_benefitemployee" style="padding: 2px; color:red;"></div>
                                <select class="form-control selectWrapper" name="benefitemployee[]"
                                id="benefitemployee" multiple="multiple" >
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;"
                                class="col-sm-3 col-form-label">Address</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_address" style="padding: 2px; color:red;"></div>
                                <textarea style="border-radius: 15px;"
                                    class="form-control form-control-sm" name="address"
                                    id="address" placeholder="Address" aria-label="Address"
                                    ></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-success btn-fw"
                    style="border-radius: 21px;">Submit</button>
                <button type="button" class="btn btn-outline-danger btn-fw"
                    style="border-radius: 21px;" data-dismiss="modal"
                    id="btnCloseModalEmployeeInsert">Cancel</button>
            </div>
        </form>
    </div>
</div>
</div>
<!-- Modal End Insert Data Employee -->

<!-- Modal Start Update Data Employee -->
<div class="modal fade" id="showModalUpdateEmployee" tabindex="-1" role="dialog"
aria-labelledby="EmployeeModalLabel" aria-hidden="true">
<div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="employeeModalLabel">Employee</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form class="forms-sample" method="POST" action="#" id="formUpdateEmployee">
            <input type="hidden" value="{{ csrf_token() }}" name="_token" />
            <input type="hidden" id="idEmployee" name="id" >
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Name
                                    <span class="" style="color:red;">*</span>
                            </label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_u_name" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="text"
                                    class="form-control form-control-sm" name="name"
                                    id="Updatename" placeholder="Employee Name"
                                    aria-label="Employee Name"
                                    oninput="this.value = this.value.toUpperCase()"  />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_u_email" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="email"
                                    class="form-control form-control-sm" name="email"
                                    id="Updateemail" placeholder="Email"
                                    aria-label="Email" value=""  />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Password</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_u_password" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="password"
                                    class="form-control form-control-sm" name="password"
                                    id="Updatepassword" placeholder="password"
                                    aria-label="password" value=""  />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Re-Password</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_u_repassword" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="password"
                                    class="form-control form-control-sm" name="repassword"
                                    id="Updaterepassword" placeholder="repassword"
                                    aria-label="repassword" value=""  />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;"
                                class="col-sm-3 col-form-label">Gender
                            </label>
                            <div class="col-sm-8" >
                                <select class="form-control selectWrapper" name="employeegender"
                                id="Updateemployeegender" >
                                    <option value="">Select Gender</option>
                                    <option value="1">Man</option> 
                                    <option value="0">Women</option> 
                                </select>
                            </div>
                            <div class="text-left" id="error_u_employeegender" style="padding: 2px; color:red;"></div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;"
                                class="col-sm-3 col-form-label">Date of Birth</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_u_date_of_birth" style="padding: 2px; color:red;"></div>
                                <input style="border-radius: 15px;" type="date"
                                    class="form-control form-control-sm" name="date_of_birth"
                                    id="Updatedate_of_birth" placeholder="date_of_birth" aria-label="date_of_birth"
                                    value=""  />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;"
                                class="col-sm-3 col-form-label">Position
                            </label>
                            <div class="text-left" id="error_u_position" style="padding: 2px; color:red;"></div>
                            <div class="col-sm-8" >
                                <select class="form-control selectWrapper" name="position"
                                id="Updateposition" >
                                    <option value="">Select Position</option>
                                    <option value="superadmin">Super Admin</option>
                                    <option value="employee">Employee</option> 
                                    <option value="finance">Finance</option> 
                                </select>
                            </div>    
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;" class="col-sm-3 col-form-label">Benefit</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_u_benefitemployee" style="padding: 2px; color:red;"></div>
                                <select class="form-control selectWrapper" name="benefitemployee[]"
                                id="Updatebenefitemployee" multiple="multiple" >
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group row">
                            <label style="padding-top: 5px;"
                                class="col-sm-3 col-form-label">Address</label>
                            <div class="col-sm-9">
                                <div class="text-left" id="error_u_address" style="padding: 2px; color:red;"></div>
                                <textarea style="border-radius: 15px;"
                                    class="form-control form-control-sm" name="address"
                                    id="Updateaddress" placeholder="Address" aria-label="Address"
                                    ></textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-outline-success btn-fw"
                    style="border-radius: 21px;">Update</button>
                <button type="button" class="btn btn-outline-danger btn-fw"
                    style="border-radius: 21px;" data-dismiss="modal"
                    id="btnCloseModalEmployeeUpdate">Cancel</button>
            </div>
        </form>
    </div>
</div>
</div>
<!-- Modal End Update Data Employee -->
@endsection
