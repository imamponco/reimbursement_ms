@extends('layouts.dashboard')
@section('content')
<!-- Tittle -->
<div class="row">
    <div style="margin-bottom: 0.2rem;" class="col-12 grid-margin">
        <div style="height: 65px; border-radius: 15px;" class="card">
            <div class="card-body">
                <div style="display: flex; align-items: flex-start; justify-content: space-between; height: 40px;">
                    <h4 class="card-title" style="font-size: 2rem; font-weight: 300; margin-bottom: 0px;">Employee</h4>
                    {{-- <button type="button" class="btn btn-success btn-rounded btn-icon" data-toggle="tooltip" data-placement="top" data-custom-class="tooltip-success" title="EDIT" data-target="#showModalUpdateAssetClass" style="margin-right:5px;"><i style="font-size:1.5rem; margin-left:-7px;" class="ti-pencil-alt"></i></button> --}}
                    <button style="margin: -0.2rem .05rem -1rem auto;" type="button" class="btn btn-primary btn-sm"
                        data-toggle="modal" data-target="#showModalInsertEmployee"><i
                            class="ti-plus menu-icon"></i>&ensp;
                        ADD</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Table -->
<div class="row">
    <div style="margin-bottom: 0.2rem;" class="col-12 grid-margin">
        <div class="card" style="border-radius: 15px;">
                <div class="card-body">
                {{-- <h4 class="card-title">Data table</h4> --}}
                <div class="row">
                    <div class="col-12">
                    <div class="table-responsive">
                        <table id="employeeTable" class="table">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Gender</th>
                                <th>Date of Birth</th>
                                <th>Email</th>
                                <th>Address</th>
                                <th>Position</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        </table>
                    </div>
                    </div>
                </div>
                </div>
            </div>
    </div>
</div>
{{-- Table End --}}
@endsection

@section('jscustom')

<script>
    
    var mTable;

    $(document).ready(function () {
        //INITIALISE SELECT2
        get_benefit();
        $("#benefitemployee").select2({
            tags: true,
            width: '19rem',
            dropdownParent: $("#formInputEmployee")
        });

        $("#Updatebenefitemployee").select2({
            tags: true,
            width: '19rem',
            dropdownParent: $("#formUpdateEmployee")
        });
        
        //RENDER DATA TABLES
        mTable = $('#employeeTable').DataTable({
            responsive: true,
            processing: true,
            "language": {
                "processing": "<div class='dot-opacity-loader'></div>"
            },
            "order": [[ 0, "desc" ]],
            serverSide: true,
            ajax: "{{ route('employee.create')}}",
            dom: 'Bfrtip',
            buttons: [
              
            ],
            columns: [
                {  
                    data: 'id_employee',
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {
                    data: 'name',
                    name: 'name'
                },
                {
                    data: 'date_of_birth',
                    name: 'date_of_birth'
                },
                {  
                    data: 'gender',
                    render: function (data, type, row, meta) {
                        return (data == 1 ? 'Man': 'Women');
                    }
                },
                {
                    data: 'email',
                    name: 'email'
                },
                {
                    data: 'address',
                    name: 'address'
                },
                {
                    data: 'position',
                    name: 'position'
                },
                {
                    data: 'id_employee',
                    render: function (data, type, row) {
                        let buttonEdit =
                            '<button type="button" class="btn btn-success btn-rounded btn-icon" data-toggle="modal" data-placement="buttom" data-custom-class="tooltip-success" title="EDIT" data-target="#showModalUpdateEmployee" style="margin-right:5px;" onclick="buttonEdit(\'' + data + '\');"><i style="font-size:1.5rem; margin-left:-7px;" class="ti-pencil-alt"></i></button>';
                        let buttonHapus =
                            '<button type="button" class="btn btn-danger btn-rounded btn-icon" data-toggle="tooltip" data-placement="bottom" data-custom-class="tooltip-success" title="DELETE" onclick="delete_data(\''+
                            data +'\',\'employee\');"><i style="font-size:1.5rem; margin-left:-8px;" class="ti-trash"></i></button>';
                        return buttonEdit + buttonHapus;
                    }
                }
            ]
        });

        //INSERT Departement
        $('#formInputEmployee').on('submit', function (e) {
            e.preventDefault();

            var url,data,form;

            url = 'employee';           //For routing
            data = $(this).serialize();    // Data Form
            form = 'Employee';          //Reset Trigger and Close Modal

            // General form insert_data(url,data,form)
            insert_data(url,data,form);
        });

        //UPDATE Departement
        $('#formUpdateEmployee').on('submit', function (e) {

            e.preventDefault();
            var url,data,form;

            url = 'employee';           //For routing
            data = $(this).serialize(); // Data Form
            form = 'Employee';          // Reset Trigger and Close Modal

            // General form update_data(url,data,form)
            update_data(url,data,form);
        });

    });

    //
    function get_benefit(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });
        $.ajax({
            type: "GET",
            url: "{{route('benefit.create')}}",
            success: function (data) {
                $.each(data.data, function (index, value) {
                    $('#benefitemployee').append('<option id=' + value.id_benefit + ' value=' + value.id_benefit +
                        '>' + value.benefit_name +'</option>')
                });
                $.each(data.data, function (index, value) {
                    $('#Updatebenefitemployee').append('<option id=' + value.id_benefit + ' value=' + value.id_benefit +
                        '>' + value.benefit_name +'</option>')
                });
            },
            error: function(response) {
                try {
                    $.toast({
                        heading: 'Danger',
                        text: 'Gagal mendapatkan data terkait.',
                        showHideTransition: 'slide',
                        icon: 'error',
                        loaderBg: '#f2a654',
                        position: 'top-right'
                    })
                } catch (err) {

                }
            }
        });
    }

    //EDIT BUTTON VIEW
    function buttonEdit(id) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $("input[name='_token']").val()
            }
        });
        $.ajax({
            type: "GET",
            url: "{{ route('employee.edit',['employee'=>1]) }}",
            data: {
                id: id
            },
            success: function (data) {
                if (data) {

                    clear_error();

                    let current_benefit = data.objtrbenefit;
                    //Set benefit values
                    let selectedValues = new Array();
                    let index = 0;
                    
                    $("#idEmployee").val(data.id_employee);
                    $("#Updatename").val(data.name)
                    $("#Updateemail").val(data.email)
                    $("#Updateemployeegender").val(data.gender)
                    $("#Updatedate_of_birth").val(data.date_of_birth)
                    $("#Updateposition").val(data.position)
                    $("#Updateaddress").val(data.address)

                    $.each(current_benefit, function (index, value) {
                        selectedValues[index] = value.id_benefit
                        index++;
                    });
                    
                    $('#Updatebenefitemployee').val(selectedValues).change();
                } 
            },
            error: function(response) {
                try {
                    $.toast({
                        heading: 'Danger',
                        text: 'Gagal mendapatkan data terkait.',
                        showHideTransition: 'slide',
                        icon: 'error',
                        loaderBg: '#f2a654',
                        position: 'top-right'
                    })
                } catch (err) {

                }

            }
        });
    }

</script>

@endsection

@include('master.employee.employee_modal')